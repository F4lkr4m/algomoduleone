/*
 * 2_3. Даны два массива неповторяющихся целых чисел, упорядоченные по возрастанию. A[0..n-1] и B[0..m-1]. n >> m.
 * Найдите их пересечение.
Требования: Время работы: O(m * log k), где k - позиция элемента B[m-1] в массиве A.. В процессе поиска
 очередного элемента B[i] в массиве A пользуйтесь результатом поиска элемента B[i-1]. Внимание!
 В этой задаче для каждого B[i] сначала нужно определить диапазон для бинарного поиска размером порядка k
 с помощью экспоненциального поиска, а потом уже в нем делать бинарный поиск.
 *
 * Щербаков Сергей, номер в ведомости 50
 * */

#include <iostream>
#include <cassert>

class Array {
public:
    Array();
    explicit Array(int64_t size);
    Array(const Array&) = delete;
    ~Array();

    void InputArray();
    void PushBack(int64_t value);
    void PrintArray();

    int64_t getCurrentSize() const;
    int64_t getElem(const int64_t index);
    int64_t& operator[] (const int64_t index);

private:
    int64_t* array;
    int64_t next;
    int64_t fullSize;
};

// Constructors && Destructors

Array::Array() {
    array = nullptr;
    next = 0;
    fullSize = 0;
}

Array::Array(int64_t inputSize) {
    assert(inputSize > 0);

    array = new int64_t[inputSize];

    fullSize = inputSize;
    // firstly Array is empty
    next = 0;
}

Array::~Array() {
    delete[] array;
}

// Public methods

void Array::InputArray() {
    assert(fullSize > 0);
    assert(array != nullptr);

    for (next = 0; next < fullSize; ++next) {
        std::cin >> array[next];
    }
}

void Array::PushBack(int64_t value) {
    assert(next < fullSize);
    assert(fullSize > 0);
    assert(array != nullptr);

    array[next++] = value;
}

void Array::PrintArray() {
    assert(array != nullptr);
    assert(next >= 0);

    for (int64_t i = 0; i < next; ++i) {
        std::cout << array[i] << " ";
    }
    std::cout << "\n";
}


int64_t Array::getCurrentSize() const {
    return next;
}

int64_t Array::getElem(int64_t index) {
    assert(index >= 0 && index < next);
    return array[index];
}

int64_t& Array::operator[] (const int64_t index) {
    assert(index >= 0 && index < next);
    return array[index];
}



int64_t getLeftPositionByExponentialSearch(Array& array,
                                           int64_t firstElementIndex,
                                           int64_t value) {
    // if firstElementIndex = 0 then 0 * 2 = 0
    if (firstElementIndex == 0) {
        if (array[firstElementIndex] == value) {
            return firstElementIndex;
        }
        ++firstElementIndex;
    }

    int64_t leftBorder = firstElementIndex;
    for (int64_t i = firstElementIndex; i < array.getCurrentSize();) {
        if (array[i] <= value) {
            leftBorder = i;
            i *= 2;
        } else {
            return leftBorder;
        }
    }
    return leftBorder;
}

int64_t binarySearch(Array& array, int64_t left, int64_t right, int64_t value) {
    /* returns -1 if not found */
    if (value > array[right]) {
        return -1;
    }

    if (value < array[left]) {
        return -1;
    }

    int64_t mid = (left + right) / 2;

    do {
        if (array[mid] == value) {
            return mid;
        } else if (value < array[mid]) {
            right = mid - 1;
        } else {
            left = mid + 1;
        }

        mid = (left + right) / 2;
    } while (left < right);

    if (array[mid] == value) {
        return mid;
    }

    return -1;
}

void getCrossing(Array& mainSeq, Array& subSeq, Array& resultSeq) {
    int64_t currentWidthPosition = 0;  // left border for exp search
    for (int64_t i = 0; i < subSeq.getCurrentSize(); ++i) {
        // get new left border
        currentWidthPosition = getLeftPositionByExponentialSearch(mainSeq,
                                                                  currentWidthPosition,
                                                                  subSeq[i]);

        // check is it in array
        int64_t findResult = binarySearch(mainSeq,
                                          currentWidthPosition,
                                          mainSeq.getCurrentSize() - 1,
                                          subSeq[i]);
        // add if was found
        if (findResult != -1) {
            resultSeq.PushBack(subSeq[i]);
            // we make next bin search from last found elem
            currentWidthPosition = findResult;
        }
    }
}

int main() {
    // input data
    int64_t n;
    std::cin >> n;
    int64_t m;
    std::cin >> m;

    Array A(n);
    A.InputArray();
    Array B(m);
    B.InputArray();

    // max size of result array is subSeq size
    Array result(m);

    // do task
    getCrossing(A, B, result);
    result.PrintArray();

    return 0;
}
