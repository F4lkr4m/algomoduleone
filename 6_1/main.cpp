/*
 * 6_1. Реализуйте стратегию выбора опорного элемента “медиана трёх”.
 * Функцию Partition реализуйте методом прохода двумя итераторами от начала массива к концу.
 *
 * Щербаков Сергей, номер 50 в ведомости
 */

#include <iostream>

// Comparator interface
template <class T>
class Comparator {
public:
    virtual bool isLess(const T&, const T&) = 0;
};

// Default comparator class
template <class T>
class CmpDefault: public Comparator<T> {
    bool isLess(const T&, const T&) override;
};

// Default comparator isLess realisation
template <class T>
bool CmpDefault<T>::isLess(const T& l, const T& r) {
    return l < r;
}


// Searcher class
template <class T>
class Searcher {
public:
    Searcher();
    Searcher(T* array, int size, Comparator<T>* isLess);
    Searcher(const Searcher&) = delete;
    ~Searcher();

    T findKStat(int k);

private:
    T* array;
    int size;

    int partition(int left, int right);
    void copyInputArray(T* inputArray, int inputSize);
    int medianIndex(int indexOne, int indexTwo, int indexThree);

    // pointer to comparator class
    Comparator<T>* comparator;
};

// Constructors && Destructor

template <class T>
Searcher<T>::Searcher() {
    array = nullptr;
    size = 0;
    comparator = nullptr;
}

template <class T>
Searcher<T>::Searcher(T* inputArray, int inputSize, Comparator<T>* isLess) {
    array = nullptr;
    size = inputSize;
    comparator = isLess;
    copyInputArray(inputArray, inputSize);
}

template <class T>
Searcher<T>::~Searcher() {
    delete[] array;
}

// Public methods

template <class T>
T Searcher<T>::findKStat(int k) {
    int left = 0;
    int right = size - 1;
    while (true) {
        int pivotPos = partition(left, right);

        if (pivotPos == k) {
            return array[pivotPos];
        }

        if (pivotPos < k) {
            left = pivotPos + 1;
            continue;
        }

        if (pivotPos > k) {
            right = pivotPos - 1;
        }
    }
}

// Private methods

template <class T>
void Searcher<T>::copyInputArray(T* inputArray, int inputSize) {
    delete[] array;

    array = new T[inputSize];

    for (int i = 0; i < size; ++i) {
        array[i] = inputArray[i];
    }
}

template <class T>
int Searcher<T>::partition(int left, int right) {
    if (left == right) {
        return left;
    }

    // get pivot index from three elements median
    int pivotIndexFromMedian = medianIndex(left, (left + right) / 2, right);
    // swap last element with pivot element
    std::swap(array[pivotIndexFromMedian], array[right]);
    const T& pivot = array[right];

    // left is begin
    int i = left;
    int j = left;

    // go to the first element bigger than pivot
    for (; comparator->isLess(array[j], pivot); ++j) {}
    for (; comparator->isLess(array[i], pivot); ++i) {}

    // and analyze next elements
    while (j < right) {
        if (comparator->isLess(array[j], pivot)) {
            std::swap(array[i], array[j]);
            ++i;
        }
        ++j;
    }

    // set pivot on right position in array
    std::swap(array[i], array[right]);
    return i;
}


template <class T>
int Searcher<T>::medianIndex(int indexOne, int indexTwo, int indexThree) {  // a b c
    // Returns index of median
    if (comparator->isLess(array[indexOne], array[indexTwo])) {  // a < b
        if (comparator->isLess(array[indexThree], array[indexOne])) {  // a > c
            return indexOne;  // a
        }
        // a < b; a < c
        if (comparator->isLess(array[indexTwo], array[indexThree])) {  // b < c
            return indexTwo;  // b
        }
        return indexThree;  // c
    }

    // a > b
    if (comparator->isLess(array[indexOne], array[indexThree])) {  // a < c
        return indexOne;  // a
    }
    // a > c; a > b
    if (comparator->isLess(array[indexThree], array[indexTwo])) {  // c < b
        return indexTwo;  // b
    }
    return indexThree;  // c
}

int main() {
    // input
    int n = 0;
    std::cin >> n;
    int k = 0;
    std::cin >> k;

    int* array = new int[n];

    for (int i = 0; i < n; ++i) {
        std::cin >> array[i];
    }

    // class creation and run task function
    CmpDefault<int> cmp;
    Searcher<int> searcher(array, n, &cmp);
    std::cout << searcher.findKStat(k);


    // memory allocation
    delete[] array;

    return 0;
}
