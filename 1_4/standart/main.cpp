/*“Считалочка”. В круг выстроено N человек, пронумерованных числами от 1 до N. Будем исключать каждого k-ого до тех пор,
 * пока не уцелеет только один человек. (Например, если N=10, k=3, то сначала умрет 3-й, потом 6-й, затем 9-й,
 * затем 2-й, затем 7-й, потом 1-й, потом 8-й, за ним - 5-й, и потом 10-й. Таким образом, уцелеет 4-й.)
 * Необходимо определить номер уцелевшего.
 * N, k ≤ 10000.
 * Требования:  Решить перебором.
*/


#include <iostream>
#include <cassert>

int countPositionOfSurvivor(int n, int k) {
    /*
     * Starting with one player
     * 1 -> 1 (pos: 0)
     * 1 2 -> 2 (pos: 1) => 0 + 3 % 2 = 1
     * 1 2 3 -> 1 2 -> 2 (pos: 1) => (1 + 3) % 3 = 1
     * 1 2 3 4 -> 1 2 4 -> 1 4 -> 1 (pos: 0) => (1 + 3) % 4 = 0
     * 1 2 3 4 5 -> 1 2 4 5 ->  2 4 5 -> 2 4 -> 4 (pos: 3) => (0 + 3) % 5 = 3
     * 1 2 3 4 5 6 -> 1 2 4 5 6 -> 1 2 4 5 -> 1 2 5 -> 1 5 -> 1 (pos: 0) => (3 + 3) % 6 = 0
     * 1 2 3 4 5 6 7 -> 1 2 4 5 6 7 -> 1 2 4 5 7 -> 1 4 5 7 -> 1 4 5 -> 1 4 -> 4 (pos: 3) => (0 + 3) % 6 = 3
     */
    assert(n > 0);

    int survivorPosition = 0;
    for (int i = 1; i <= n; ++i) {
        survivorPosition += k;
        survivorPosition %= i;
    }
    // And + 1 for humans
    return survivorPosition + 1;
}

int main() {
    int n = 0;
    std::cin >> n;
    int k = 0;
    std::cin >> k;

    std::cout << countPositionOfSurvivor(n, k);

    return 0;
}
