/*
 * “Считалочка”. В круг выстроено N человек, пронумерованных числами от 1 до N. Будем исключать каждого k-ого
 * до тех пор, пока не уцелеет только один человек. (Например, если N=10, k=3, то сначала умрет 3-й, потом 6-й,
 * затем 9-й, затем 2-й, затем 7-й, потом 1-й, потом 8-й, за ним - 5-й, и потом 10-й. Таким образом, уцелеет 4-й.)
 * Необходимо определить номер уцелевшего.
N, k ≤ 10000.
Требования:  Решить перебором.
 *
 * Щербаков Сергей, номер в ведомости 50
 */

#include <iostream>
#include <cassert>

struct Node {
    int number;
    Node* next;
    Node() {
        number = 0;
        next = nullptr;
    }

    explicit Node(int value) {
        number = value;
        next = nullptr;
    }
};

class SurvivorList {
public:
    SurvivorList();
    SurvivorList( const SurvivorList&) = delete;
    ~SurvivorList();

    void addNode(int value);
    Node* deleteNode(Node* prev, Node* nodeToDelete);
    void fillInOrderList(int n);
    int getNumberOfSurvivor(int n, int k);

private:
    Node* head;
    Node* tail;
    int size;
};

// Constructors && Destructors

SurvivorList::SurvivorList() {
    head = nullptr;
    tail = nullptr;
    size = 0;
}

SurvivorList::~SurvivorList() {
    tail->next = nullptr;
    while (head) {
        Node* tmpNode = head;
        head = head->next;
        delete tmpNode;
    }
}

// Public methods

void SurvivorList::addNode(int value) {
    // add Node in List
    Node* newNode = new Node(value);
    if (head == nullptr) {
        head = newNode;
        tail = newNode;
    } else {
        tail->next = newNode;
        tail = newNode;
        newNode->next = head;
    }
    ++size;
}

Node* SurvivorList::deleteNode(Node* prev, Node* nodeToDelete) {
    /*
     * deleting Node and return the next after this
     * before:  Node0 -> Node1 -> Node2
     * after:   Node0 -> Node2
     * returns: Node2
     */

    if (nodeToDelete == head) {
        head = nodeToDelete->next;
    } else if (nodeToDelete == tail) {
        tail = prev;
    }
    prev->next = nodeToDelete->next;
    --size;
    delete nodeToDelete;
    return prev->next;
}

void SurvivorList::fillInOrderList(int n) {
    // fill List with numbers from 1 to n

    for (int i = 1; i <= n; ++i) {
        addNode(i);
    }
}

int SurvivorList::getNumberOfSurvivor(int n, int k) {
    // Filling List, delete every k node and return the last one

    fillInOrderList(n);

    Node* curNode = head;
    Node* prevNode = tail;

    while (size > 1) {
        for (int i = 0; i < k - 1; ++i) {
            prevNode = curNode;
            curNode = curNode->next;
        }
        curNode = deleteNode(prevNode, curNode);
    }

    return curNode->number;
}

int main() {
    // Data input
    int n;
    std::cin >> n;
    int k;
    std::cin >> k;

    assert(n > 0);

    // do task
    SurvivorList List;
    std::cout << List.getNumberOfSurvivor(n, k);

    return 0;
}
