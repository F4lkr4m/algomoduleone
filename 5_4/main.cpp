/*
 * 5_4. Закраска прямой 2.
 * На числовой прямой окрасили N отрезков. Известны координаты левого и правого концов
 * каждого отрезка (Li и Ri). Найти сумму длин частей числовой прямой, окрашенных ровно в один слой.
 *
 * Щербаков Сергей, номер  в ведомости 50
 */

#include <iostream>
#include <cstring>
#include <cassert>


struct Point {
    // Coordinate of point
    int x;
    // Bool variable -> point is begin of sector or not
    bool isBegin;
    Point(int x, bool isBegin): x(x), isBegin(isBegin) {};
    Point(): x(0), isBegin(false) {};
};

template <class T>
void Merge(T* firstArray, int firstLen, T* secondArray, int secondLen, T* buffer, bool isLess(const T&, const T&)) {
    // Merging two arrays

    // k -> index iterator for buffer
    int k = 0;
    // i -> index iterator for first array
    int i = 0;
    // j -> index iterator for second array
    int j = 0;
    for (; i < firstLen && j < secondLen; ++k) {
        // setting elements in buffer while j and i theirs sizes
        if (isLess(firstArray[i], secondArray[j])) {
            buffer[k] = firstArray[i];
            ++i;
        } else {
            buffer[k] = secondArray[j];
            ++j;
        }
    }

    // set other elements
    while (i < firstLen) {
        buffer[k++] = firstArray[i++];
    }

    while (j < secondLen) {
        buffer[k++] = secondArray[j++];
    }
}

template <class T>
void MergeSort(T* array, int size, bool isLess(const T&, const T&)) {
    if (size <= 1) {
        return;
    }

    int firstLen = size / 2;
    int secondLen = size - firstLen;

    // recursion call
    MergeSort(array, firstLen, isLess);
    MergeSort(array + firstLen, secondLen, isLess);


    // creating buffer for merging
    T* buffer = new T[size];
    Merge(array, firstLen, array + firstLen, secondLen, buffer, isLess);
    // copy in original array
    memcpy(array, buffer, sizeof(T) * size);

    // allocating buffer
    delete[] buffer;
}

template <class T>
bool isLessStd(const T& l, const T& r) {
    // standard compare function
    return l < r;
}

bool isLessByX(const Point& l, const Point& r) {
    // function for by x point compare
    return l.x < r.x;
}

int getLengthOfOneLayerPaint(Point* array, int size) {

    // firstly sort array of points by x
    MergeSort(array, size, isLessByX);

    // result variable
    int result = 0;

    // take first point
    int numberOfLayers = 1;
    assert(array[0].isBegin == true);
    int currentCoordinate = array[0].x;

    for (int i = 1; i < size; ++i) {
        // remember if was one layer
        bool isOneLayerBefore = false;
        if (numberOfLayers == 1) {
            isOneLayerBefore = true;
        }

        // check next point
        if (array[i].isBegin) {
            ++numberOfLayers;
        } else {
            --numberOfLayers;
        }

        if (numberOfLayers == 1) {
            // if one layer sector is start, we remember point
            currentCoordinate = array[i].x;
        } else if (isOneLayerBefore) {
            // if one layer is change -> we can sum the length of sector
            result += array[i].x - currentCoordinate;
        }
    }

    // return sum of one layer sectors
    return result;
}


int main() {

    // data input
    int n = 0;
    std::cin >> n;
    auto* pointArray = new Point[n * 2];

    for (int i = 0; i < n * 2; ++i) {
        int coordinate = 0;
        std::cin >> coordinate;
        pointArray[i].x = coordinate;
        pointArray[i].isBegin = !(i % 2);
    }

    // do task and out

    std::cout << getLengthOfOneLayerPaint(pointArray, n * 2);


    // memory allocating
    delete[] pointArray;

    return 0;
}
