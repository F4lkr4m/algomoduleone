/*
 * 4_1
 * Вовочка ест фрукты из бабушкиной корзины. В корзине лежат фрукты разной массы.
 * Вовочка может поднять не более K грамм. Каждый фрукт весит не более K грамм. За раз
 * он выбирает несколько самых тяжелых фруктов, которые может поднять одновременно, откусывает
 * от каждого половину и кладет огрызки обратно в корзину. Если фрукт весит нечетное число грамм,
 * он откусывает большую половину. Фрукт массы 1гр он съедает полностью.
 *
 * Определить за сколько подходов Вовочка съест все фрукты в корзине.
 *
 * Формат входных данных. Вначале вводится n - количество фруктов и n строк с массами фруктов. Затем K - "грузоподъемность".
 *
 * Формат выходных данных. Неотрицательное число - количество подходов к корзине.
 *
 * Щербаков Сергей, номер в ведомости 50
 */

#include <iostream>
#include <cassert>

template <class T>
class CmpDefault {
public:
    bool isLess(const T&, const T&);
};

// Default comparator isLess realisation
template <class T>
bool CmpDefault<T>::isLess(const T& l, const T& r) {
    return l < r;
}

// Heap class
template <class T, class Comparator>
class Heap {
public:
    Heap();
    explicit Heap(Comparator inputComparator);
    Heap(T* inputArray, int inputSize, Comparator inputComparator);
    Heap(const Heap&) = delete;
    Heap operator=(Heap heap) = delete;
    ~Heap();

    T extractMax();

    void insert(T elem);
    int peekMax() const;
    int getActiveSize();
    bool isEmpty();

private:
    T* array;
    int activeSize;
    int size;
    Comparator comparator;

    void buildHeap();
    void siftDown(int i);
    void siftUp(int i);
    void resizeArray();
    void arrayCopy(T* to, T* from);

};

// Constructors && Destructors

template <class T, class Comparator>
Heap<T, Comparator>::Heap () {
    array = new T[1];
    size = 1;
    activeSize = 0;
    comparator = CmpDefault<T>();
}

template <class T, class Comparator>
Heap<T, Comparator>::Heap(const Comparator inputComparator) {
    array = new T[1];
    size = 1;
    activeSize = 0;
    comparator = inputComparator;
}

template <class T, class Comparator>
Heap<T, Comparator>::Heap(T* inputArray, int inputSize, const Comparator inputComparator) {
    array = inputArray;
    activeSize = inputSize;
    size = inputSize;
    comparator = inputComparator;
    buildHeap();
}

template <class T, class Comparator>
Heap<T, Comparator>::~Heap() {
    delete[] array;
}

// Public methods

template <class T, class Comparator>
T Heap<T, Comparator>::extractMax() {
    assert(activeSize != 0);

    T result = array[0];

    array[0] = array[--activeSize];

    if (activeSize != 0) {
        siftDown(0);
    }
    return result;
}

template <class T, class Comparator>
void Heap<T, Comparator>::insert(T elem) {
    if (activeSize + 1 == size) {
        resizeArray();
    }

    array[activeSize++] = elem;
    siftUp(activeSize - 1);
}

template <class T, class Comparator>
void Heap<T, Comparator>::buildHeap() {
    for (int i = size / 2 - 1; i >= 0; --i) {
        siftDown(i);
    }
}

template <class T, class Comparator>
int Heap<T, Comparator>::peekMax() const {
    assert(activeSize != 0);
    return array[0];
}

template <class T, class Comparator>
int Heap<T, Comparator>::getActiveSize() {
    return activeSize;
}

template <class T, class Comparator>
bool Heap<T, Comparator>::isEmpty() {
    return activeSize == 0;
}

// Private Methods

template <class T, class Comparator>
void Heap<T, Comparator>::siftDown(int i) {
    int left = 2 * i + 1;
    int right = 2 * i + 2;

    int largest = i;
    if (left < activeSize && comparator.isLess(array[i], array[left])) {
        largest = left;
    }
    if (right < activeSize && comparator.isLess(array[largest], array[right])) {
        largest = right;
    }

    if (largest != i) {
        T tmp = array[i];
        array[i] = array[largest];
        array[largest] = tmp;
        siftDown(largest);
    }
}

template <class T, class Comparator>
void Heap<T, Comparator>::siftUp(int i) {
    while (i > 0) {
        int parent = (i - 1) / 2;
        if (!comparator.isLess(array[parent], array[i])) {
            return;
        }

        T tmp = array[i];
        array[i] = array[parent];
        array[parent] = tmp;

        i = parent;
    }
}

template <class T, class Comparator>
void Heap<T, Comparator>::resizeArray() {
    T* tmpArray = array;
    array = new T[size * 2];
    arrayCopy(array, tmpArray);
    size *= 2;
    delete[] tmpArray;
}

template <class T, class Comparator>
void Heap<T, Comparator>::arrayCopy(T* to, T* from) {
    for (int i = 0; i < size; ++i) {
        to[i] = from[i];
    }
}

int numOfGreedyIterations(int* fruitArray, int n, int k) {
    Heap<int, CmpDefault<int>> heap(fruitArray, n, CmpDefault<int>());

    int result = 0;
    // array for carrying fruits
    int* carry_fruits = new int[heap.getActiveSize()];
    // index iterator for this array (ind)
    int ind = 0;
    // weight in hands
    int carry_weight = 0;

    while (!heap.isEmpty()) {
        // take fruits while can take it
        while (!heap.isEmpty() && (carry_weight + heap.peekMax() <= k)) {
            carry_fruits[ind] = heap.extractMax();
            carry_weight += carry_fruits[ind];
            ++ind;
        }
        // return the half of fruit weight
        for (int i = 0; i < ind; ++i) {
            if (carry_fruits[i] != 1) {
                heap.insert(carry_fruits[i] / 2);
            }
        }

        // refresh variables
        ++result;
        carry_weight = 0;
        ind = 0;
    }

    // allocate fruits array
    delete[] carry_fruits;

    return result;
}

int main() {
    // data input
    int n = 0;
    std::cin >> n;

    int* fruitArray = new int[n];
    for (int i = 0; i < n; ++i) {
        std::cin >> fruitArray[i];
    }

    int k = 0;
    std::cin >> k;

    // do task and print result
    std::cout << numOfGreedyIterations(fruitArray, n , k);

    return 0;
}
