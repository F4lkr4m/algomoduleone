/*
 *
 * Во всех задачах из следующего списка следует написать структуру данных, обрабатывающую команды push* и pop*.
Формат входных данных.
В первой строке количество команд n. n ≤ 1000000.
Каждая команда задаётся как 2 целых числа: a b.
a = 1 - push front
a = 2 - pop front
a = 3 - push back
a = 4 - pop back
Команды добавления элемента 1 и 3 заданы с неотрицательным параметром b.
Для очереди используются команды 2 и 3. Для дека используются все четыре команды.
Если дана команда pop*, то число b - ожидаемое значение. Если команда pop вызвана для пустой структуры данных, то ожидается “-1”.
Формат выходных данных.
Требуется напечатать YES - если все ожидаемые значения совпали. Иначе, если хотя бы одно ожидание не оправдалось, то напечатать NO.

 3_2. Реализовать дек с динамическим зацикленным буфером.
Требования: Дек должен быть реализован в виде класса.
 *
 * Щербаков Сергей, номер 50 в ведомости
 */

#include <iostream>
#include <cassert>

template <class T>
class Deque {
public:
    Deque();
    Deque(const Deque&) = delete;
    ~Deque();

    bool isEmpty();
    void pushBack(T value);
    T popBack();
    void pushFront(T value);
    T popFront();

private:
    void arrayCopy(T* from, T* to);
    void resize();


    T* array;
    int head;
    int tail;
    int size;
    int activeSize;
};

// Constructors && Destructors

template <class T>
Deque<T>::Deque() {
    array = nullptr;
    head = 0;
    tail = 0;
    size = 0;
    activeSize = 0;
}

template <class T>
Deque<T>::~Deque() {
    delete[] array;
}

// Public methods

template <class T>
bool Deque<T>::isEmpty() {
    return activeSize == 0;
}

template <class T>
void Deque<T>::pushBack(T value) {
    if (activeSize + 1 >= size) {
        resize();
    }

    if (head == tail && activeSize == 0) {
        array[tail] = value;
    } else {
        // here just cycle index
        tail = ++tail % size;
        array[tail] = value;
    }
    ++activeSize;
}

template <class T>
T Deque<T>::popBack() {
    if (activeSize == 0) {
        return -1;
    }

    if (tail == 0) {
        tail = size;
    }

    --activeSize;
    return array[tail-- % size];
}

template <class T>
void Deque<T>::pushFront(T value) {
    if (activeSize + 1 >= size) {
        resize();
    }

    if (activeSize == 0) {
        array[head] = value;
        ++tail;
    } else {
        // if head == 0 then go cycle buffer to back
        if (head == 0) {
            head = size;
        }
        // place element
        array[--head] = value;
    }
    ++activeSize;
}

template <class T>
T Deque<T>::popFront() {
    if (activeSize == 0) {
        return -1;
    }

    T tmpVal = array[head];
    head = ++head % size;

    --activeSize;
    return tmpVal;
}

// Private methods

template <class T>
void Deque<T>::arrayCopy(T* from, T* to) {
    // coping array
    for (int i = 0; i < activeSize; ++i) {
        to[i] = from[(head + i) % size];
    }
}


template <class T>
void Deque<T>::resize() {
    // increase size of array * 2 or create array
    if (size == 0) {
        // create array
        array = new T[1];
        size = 1;
    } else {
        // increase size * 2
        T* tmpArray = array;
        array = new T[size * 2];
        arrayCopy(tmpArray, array);
        size *= 2;
        delete[] tmpArray;
    }
    // refresh variables
    head = 0;
    tail = activeSize - 1;
}

int main() {
    // input number of cmds
    int n = 0;
    std::cin >> n;

    Deque<int> deq;
    bool result = true;

    // do commands and check
    for (int i = 0; i < n; ++i) {
        int cmd = 0;
        int val = 0;
        std::cin >> cmd >> val;
        switch (cmd) {
            case 1:
                deq.pushFront(val);
                break;

            case 2:
                if (deq.isEmpty()) {
                    result = result && val == -1;
                } else {
                    result = result && deq.popFront() == val;
                }
                break;

            case 3:
                deq.pushBack(val);
                break;

            case 4:
                if (deq.isEmpty()) {
                    result = result && val == -1;
                } else {
                    result = result && deq.popBack() == val;
                }
                break;

            default:
                assert(false);
        }
    }

    std::cout << (result ? "YES" : "NO");

    return 0;
}
