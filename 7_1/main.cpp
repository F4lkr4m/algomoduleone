/*
 * 7_1. MSD для строк.
 * Дан массив строк. Количество строк не больше 10^5.
 * Отсортировать массив методом поразрядной сортировки MSD по символам.
 * Размер алфавита - 256 символов. Последний символ строки = ‘\0’
 *
 * Щербаков Сергей, номер в ведомости 50
 */

#include <iostream>
#include <cstring>
#include <cassert>

template<class T>
class Array {
public:
    Array();

    Array(const Array &) = delete;

    ~Array();

    void copyTo(Array<T> *) const;

    void pushBack(T sign);

    char getElementByIndex(int index);

    int getActiveSize();

    void setElementByIndex(int index, T val);

    void printArray();

    T& operator[] (const int index);

private:
    T *array;
    int activeSize;
    int fullSize;

    void resize();
};

// Constructors && Destructors

template<class T>
Array<T>::Array() {
    fullSize = 4;
    array = new T[fullSize];
    activeSize = 0;
}

template<class T>
void Array<T>::copyTo(Array<T> *rhs) const {
    delete[] rhs->array;

    rhs->fullSize = fullSize;
    rhs->array = new T[fullSize];
    rhs->activeSize = activeSize;
    for (int i = 0; i < activeSize; ++i) {
        rhs->array[i] = array[i];
    }
}

template<class T>
Array<T>::~Array() {
    delete[] array;
}

// Public Methods

template<class T>
void Array<T>::pushBack(T sign) {
    if (activeSize + 1 == fullSize) {
        resize();
    }

    array[activeSize++] = sign;
}

template<class T>
char Array<T>::getElementByIndex(int index) {
    assert(index < activeSize);
    return array[index];
}

template<class T>
int Array<T>::getActiveSize() {
    return activeSize;
}

template<class T>
void Array<T>::setElementByIndex(int index, T val) {
    assert(index < activeSize);
    array[index] = val;
}

template<>
void Array<char>::printArray() {
    for (int i = 0; i < activeSize - 1; ++i) {
        std::cout << array[i];
    }
}

template <class T>
T& Array<T>::operator[] (const int index) {
    assert(index < activeSize && index > -1);
    return array[index];
}

// Private Methods

template<class T>
void Array<T>::resize() {
    T *tmp = array;
    array = new T[fullSize * 2];
    for (int i = 0; i < activeSize; ++i) {
        array[i] = tmp[i];
    }

    fullSize *= 2;
    delete[] tmp;
}

// Sort

void CountingSort(Array<char> *a, int left, int right, int signNum, int *signsArray, int numOfSigns) {
    // fill array with signs in string
    for (int i = left; i < right; ++i) {
        signsArray[a[i][signNum]]++;
    }

    // iter in array
    for (int i = 1; i < numOfSigns + 1; ++i) {
        signsArray[i] += signsArray[i - 1];
    }

    // fill cpy array with right positions of signs
    auto *buffer = new Array<char>[right - left];
    for (int i = left; i < right; ++i) {
        signsArray[a[i][signNum]]--;
        a[i].copyTo(&buffer[signsArray[a[i][signNum]]]);
    }

    // cpy elements back
    for (int i = left; i < right; ++i) {
        buffer[i - left].copyTo(&a[i]);
    }

    delete[] buffer;
}

void MSDSort(Array<char> *array, int left, int right, int signNum) {
    // if one element don't sort
    if (right - left <= 1) {
        return;
    }

    // create signs array
    int numOfSigns = 256;
    int *signsArray = new int[numOfSigns + 1];
    memset(signsArray, 0, (numOfSigns + 1) * sizeof(int));

    // make sort for a letter
    CountingSort(array, left, right, signNum, signsArray, numOfSigns);

    // and for all other letters
    for (int i = 1; i < numOfSigns; ++i) {
        if (signsArray[i + 1] - signsArray[i] <= 1) {
            continue;
        }

        MSDSort(array, signsArray[i] + left, signsArray[i + 1] + left, signNum + 1);
    }

    delete[] signsArray;
}

int main() {
    // create matrix with lines
    int size = 4;
    auto arrayArrays = new Array<char>[size];
    int activeSize = 0;

    // fill lines matrix
    char c;
    for (; scanf("%c", &c) != -1;) {
        if (c == '\0' || c == '\n') {
            // add end of line ending sign
            arrayArrays[activeSize++].pushBack('\0');

            // if matrix is full just increase the size
            if (activeSize == size) {
                auto* tmp = arrayArrays;
                arrayArrays = new Array<char>[size * 2];

                for (int i = 0; i < size; ++i) {
                    tmp[i].copyTo(&arrayArrays[i]);
                }

                size *= 2;
                delete[] tmp;
            }
        } else {
            // add sign in array
            arrayArrays[activeSize].pushBack(c);
        }
    }

    // sort
    MSDSort(arrayArrays, 0, activeSize, 0);

    // print out
    for (int j = 0; j < activeSize; ++j) {
        arrayArrays[j].printArray();
        std::cout << "\n";
    }

    delete[] arrayArrays;
    return 0;
}